<?php
/**
 * Template Name: About
 */
?>

<?php
	$contentLeft = esc_html( get_post_meta( get_the_id(), 'content_left_intro', true ) );
	$contentRight = esc_html( get_post_meta( get_the_id(), 'content_right_intro', true ) );
	$contentFull = esc_html( get_post_meta( get_the_id(), 'full_content_intro', true ) );
	$subTitle = esc_html( get_post_meta( get_the_id(), 'title_team_members', true ) );
?>

<main class="about-layout">

	<section class="about-layout__intro">
		<h1 class="about-layout__title">
			<?php the_title();?>
		</h1>
		<div class="about-layout__copy">

			<?php if($contentFull){?>
				<div class="about-layout__content-full">
					<p>
						<?php echo $contentFull;?>
					</p>
				</div>
			<?php }else{?>
				
				<div class="about-layout__content-left">
					<p>
						<?php echo $contentLeft;?>
					</p>
				</div>
				<div class="about-layout__content-right">
					<p>
						<?php echo $contentRight;?>
					</p>
				</div>

			<?php }?>
			
		</div>
	</section>

	<section class="about-layout__team-members">
		<h2 class="about-layout__title">
			<?php echo $subTitle;?>
		</h2>

		<?php
			$tharonTeam = get_post_meta( get_the_ID(), 'members', true );
			if( $tharonTeam ):
		?>	
				<ul class="">
					<?php 
						for( $z = 0; $z < $tharonTeam; $z++ ):
							$memberPrefix = 'members_' . $z . '_';
						$memberName = esc_html( get_post_meta( get_the_ID(), $memberPrefix . 'name', true ) );
						$memberTitle = esc_html( get_post_meta( get_the_ID(), $memberPrefix . 'title', true ) );
						 $memberDescription = nl2br(esc_html( get_post_meta( get_the_ID(), $memberPrefix . 'description', true ) ));
						//$memberDescription = '<p>' . implode('</p><p>', array_filter(explode("\r\n", get_post_meta( get_the_ID(), 'members_' . $z . '_description', true )))) . '</p>';

						$memberIMG = (int) get_post_meta( get_the_ID(), 'members_' . $z . '_profile_pic', true );
					?>

						<li class="team-member">
							<div class="team-member__pic">
								<?php echo wp_get_attachment_image( $memberIMG,'full' ) ?>
								
							</div>
							<div class="team-member__info">
								<h2 class="">
									<?php echo $memberName;?>
								</h2>
								<span>
									<?php echo $memberTitle;?>
								</span>
								<p>
									<?php echo $memberDescription;?>
								</p>
							</div>
						</li>

					<?php endfor;?>
				</ul>

		<?php endif;?>
	</section>

	<section class="about-layout__button text-center">
		<?php
			$tharonButtons = get_post_meta( get_the_ID(), 'pages', true );
			if( $tharonButtons ):
		?>	
				
					<?php 
						for( $z = 0; $z < $tharonButtons; $z++ ):
						$buttonText = esc_html( get_post_meta( get_the_ID(), 'pages_' . $z . '_text', true ) );
						$buttonLink = get_post_meta( get_the_ID(), 'pages_' . $z . '_link', true );
					?>

						<a class="button-types button-types--main" href="<?php echo get_page_link($buttonLink );?>">
							<?php echo $buttonText;?>
						</a>

					<?php endfor;?>
				

		<?php endif;?>
	</section>

</main>