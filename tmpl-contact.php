<?php
/**
 * Template Name: Contact
 */
?>
<?php
	$pageContent = esc_html( get_post_meta( get_the_id(), 'copy_intro', true ) );
?>
<main class="contact-layout">

	<section class="contact-layout__content">
		<h1 class="contact-layout__title">
			<?php the_title();?>
		</h2>
		<div class="contact-layout__copy">
			<div class="contact-layout__content-left">
				<p>
					<?php echo $pageContent;?>
				</p>
			</div>
			<div class="contact-layout__content-right">
				<?php echo gravity_form(3, false, false, false, '', true )?>
			</div>
		</div>
	</section>

	<section class="contact-layout__button text-center">
		<?php
			$contactButtons = get_post_meta( get_the_ID(), 'pages', true );
			if( $contactButtons ):
		?>	
				
					<?php 
						for( $z = 0; $z < $contactButtons; $z++ ):
						$buttonText = esc_html( get_post_meta( get_the_ID(), 'pages_' . $z . '_text', true ) );
						$buttonLink = get_post_meta( get_the_ID(), 'pages_' . $z . '_page_link', true );
					?>

						<a class="button-types button-types--main" href="<?php echo get_page_link($buttonLink);?>">
							<?php echo $buttonText;?>
						</a>

					<?php endfor;?>
				

		<?php endif;?>
	</section>

</main>