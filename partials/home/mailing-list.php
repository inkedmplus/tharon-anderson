
<div class="mailing-list mailing-list--home">

	<h2 class="font-styles__body-copy">
		<?php echo esc_html('Receive project updates and more through our mailing list.');?>
	</h2>

	<?php echo gravity_form(1, false, false, false, '', true )?>


</div>