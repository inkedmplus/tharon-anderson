<?php 

//get relationship field
//foundation default orbit slider
$posts = get_post_meta( get_the_ID(), 'portfolio_options', true );

if( $posts ): ?>
		<div id="owl-outside-wrap">
			<div id="owlHome" class="owl-carousel owl-theme">
		        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
		            //get your acf meta fields
		            
		         ?>
		            <?php 
		            	setup_postdata($post); 
		            	$featuredIMG = get_post_meta( get_the_ID(),'featured_image_portfolio',true);
						$featuredIMG = json_decode($featuredIMG);
						$featuredIMG = $featuredIMG->cropped_image;

						$locationText = esc_html( get_post_meta( get_the_id(), 'location', true ) );
		        	?>

		        		<div class="item slider-info">
		        			<?php echo wp_get_attachment_image( $featuredIMG,'full' ) ?>
		        			<a class="slider-info__link" href="<?php the_permalink();?>">
		        				<span class="slider-info__content">
			        				<span class="slider-info__title">
			        					<?php the_title();?>
			        				</span>
			        				<span class="slider-info__location">
			        					<?php echo $locationText;?>
			        				</span>
			        			</span>
		        			</a>
		        		</div>
		            
		        <?php endforeach; ?>
	    	</div>
	    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>