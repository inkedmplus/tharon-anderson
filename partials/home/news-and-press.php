<?php
	$args = array(            
		'post_type' => 'news_and_press',
		'posts_per_page' => 3
	);
	//$query = query_posts($args);
	$newsPressHome = new WP_Query( $args );		
?>
<div class="misc-wrap misc-wrap--home">
	<div class="news-and-press news-and-press--home">
		<h2 class="news-and-press__intro-title">
			<?php echo esc_html('News & Press');?>
		</h2>

		<?php  
			while ($newsPressHome->have_posts()) : $newsPressHome->the_post();
		?>
			<?php get_template_part('partials/news-press'); ?>
		<?php 
		 	endwhile; 
			wp_reset_query();
		?>
	</div>

	
	<a href="<?php echo get_page_link(99); ?>" class="button-types button-types--main">
		<?php echo esc_html('View More');?>
	</a>
</div>