
<?php 
	$introPageLink = get_post_meta( get_the_id(), 'button_link_d_home', true );
	$introDescription = esc_html( get_post_meta( get_the_id(), 'description_home', true ) );
	$introButtonText = esc_html( get_post_meta( get_the_id(), 'button_text_d_home', true ) );
?>
<div class="intro-site">
	<h2 class="intro-site__title">
		<?php echo $introDescription;?>
	</h2>

	<a class="button-types button-types--main" href="<?php echo get_page_link($introPageLink);?>">
		<?php echo $introButtonText;?>
	</a>
</div>