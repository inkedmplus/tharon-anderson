<div class="testimonial testimonial--home testimonial--special">
	<?php 

	//get relationship field
	//foundation default orbit slider
	$posts = get_post_meta( get_the_ID(), 'show_testimonials', true );

	if( $posts ): ?>
			
			<div id="owlTestimonialHome" class="owl-carousel owl-theme">
		        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
		            //get your acf meta fields
		            
		         ?>
		            <?php 
		            	setup_postdata($post); 
						$testimonialDescription = esc_html( get_post_meta( get_the_id(), 'description', true ) );
		        	?>

		        		<div class="item">
		        			<div class="testimonial__border-box">
			        			<h2 class="testimonial__title">
			        				<?php echo $testimonialDescription;?>
			        			</h2>
			        			<span class="testimonial__cite">
			        				<?php echo esc_html('- ');?> <?php the_title();?>
			        			</span>
			        		</div>
		        		</div>
		            
		        <?php endforeach; ?>
	    	</div>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
</div>
