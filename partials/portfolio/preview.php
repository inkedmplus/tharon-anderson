<?php
	$featuredIMG = get_post_meta( get_the_ID(),'featured_image_portfolio',true);
	$featuredIMG = json_decode($featuredIMG);
	$featuredIMG = $featuredIMG->cropped_image;

	$locationText = esc_html( get_post_meta( get_the_id(), 'location', true ) );
?>

<div class="portfolio-pieces__block">
	<a class="portfolio-pieces__bg" href="<?php the_permalink();?>" style="background-image:url(<?php echo wp_get_attachment_url( $featuredIMG,'full' ) ?>);">
		<span class="portfolio-pieces__wrapper">
			<span class="portfolio-pieces__content">
				<span class="portfolio-pieces__title">
					<?php the_title();?>
				</span>
				<span class="portfolio-pieces__location">
					<?php echo $locationText;?>
				</span>
			</span>
		</span>
	</a>
</div>