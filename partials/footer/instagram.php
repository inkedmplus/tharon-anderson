<div class="instagram-slider">
	<div class="instagram-slider__intro">
		<span class="instagram-slider__sub-title">
			<?php echo esc_html('instagram');?>
		</span>
		<a class="instagram-slider__handle" href="<?php echo esc_url('https://www.instagram.com/tharonandersondesign/');?>">
			<?php echo esc_html('@TharonAndersonDesign');?>
		</a>
	</div>
	
</div>