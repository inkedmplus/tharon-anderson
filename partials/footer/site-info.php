<?php  
	$instaLinkFooter = esc_attr(get_option('options_instagram_global', true ));
	$pinterestLinkFooter = esc_attr(get_option('options_pinterest_global', true ));
	$fbLinkFooter = esc_attr(get_option('options_facebook_global', true ));
	$twitterLinkFooter = esc_attr(get_option('options_twitter_global', true ));
?>
<div class="footer-misc">
	
	<div class="footer-misc__container">
		<!-- logo -->
		<a class="footer-misc__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img class="" src="<?= get_template_directory_uri(); ?>/dist/images/footerlogoblack.png" alt="<?php bloginfo('name'); ?>">  
		</a>

		<!-- copy right -->
		<div class="footer-misc__cr">
			<span>
				<?php echo esc_html('Copyright ');?> <?php echo get_bloginfo( 'name' ); ?>
			</span>
			<a href="<?php echo esc_attr('http://projectmplus.com/');?>">
				<?php echo esc_html('Design and Development by Project M Plus');?>
			</a>
		</div>

		<!-- nav -->
		<ul class="footer-misc__menu">
			<?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
		</ul>

		<!-- social -->
		<ul class="footer-misc__social">
			<li class="footer-misc__social-item">
				<a href="<?php echo $instaLinkFooter;?>" target="_blank">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
			</li>
			<?php if(!empty($post->post_password)):?>
				<li class="footer-misc__social-item">
					<a href="<?php echo $pinterestLinkFooter;?>" target="_blank">
						<i class="fa fa-pinterest-p" aria-hidden="true"></i>
					</a>
				</li>
			<?php endif;?>
			<li class="footer-misc__social-item">
				<a href="<?php echo $fbLinkFooter;?>" target="_blank">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
			</li>
			<?php if($twitterLinkFooter):?>
				<li class="footer-misc__social-item">
					<a href="<?php echo $twitterLinkFooter;?>" target="_blank">
						<i class="fa fa-twitter" aria-hidden="true"></i>
					</a>
				</li>
			<?php endif;?>
		</ul>

	</div>

</div>