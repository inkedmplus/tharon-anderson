<?php  
	$projectName = get_post_meta( get_the_ID(), 'project_name', true );
	$projectType = get_post_meta( get_the_ID(), 'link_type', true );
	$pojectLink = get_post_meta( get_the_ID(), 'external_link', true );
	$projectPDF = get_post_meta( get_the_ID(), 'pdf_link', true );
	$projectPDF = wp_get_attachment_url( $projectPDF );

	$projectIMG = get_post_meta( get_the_ID(),'project_image',true);
	$projectIMG = json_decode($projectIMG);
	$projectIMG = $projectIMG->cropped_image;

	// $projectReg = (int) get_post_meta( get_the_ID(),'regular_image',true);
	$projectIMGReg = get_post_meta( get_the_ID(),'regular_image',true);
	$projectIMGReg = json_decode($projectIMGReg);
	$projectIMGReg = $projectIMGReg->cropped_image;

	$projectType = $projectType == 'Page' ? $pojectLink : $projectPDF;

	// $projectLogo = (int) get_post_meta( get_the_ID(),'project_logo',true);
	$projectDate = get_field('date');
?>
<div class="news-and-press__block <?php if(is_page_template( 'tmpl-press.php' )):?>news-and-press__block--press<?php endif;?>">

	<div class="news-and-press__content">
		<a href="<?php echo $projectType;?>" class="news-and-press__link-img" target="_blank">
			
			<?php if(get_field('regular_image')):?>
				<?php echo wp_get_attachment_image( $projectIMGReg,'full' ) ?>
			<?php else:?>
				<?php echo wp_get_attachment_image( $projectIMG,'full' ) ?>
			<?php endif;?>
		</a>
		
	</div>
	<a href="<?php echo $projectType;?>" class="news-and-press__link" target="_blank">
		<span class="news-and-press__info">
			<span class="news-and-press__title">
				<?php the_title();?>
			</span>
			<!-- <span class="news-and-press__name">
				<?php echo $projectName;?>
			</span> -->
		</span>

	</a>

	<?php if($projectDate):?>
		<div class="news-and-press__logo">
			<p><?php the_field('date');?></p>
		</div>
	<?php endif;?>

</div>