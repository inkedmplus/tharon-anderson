<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;
use WP_Query;
/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

class Foundation_Nav_Menu extends \Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = Array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"menu\">\n";
    }
}

//acf extras

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page('Theme Settings');

}

// check if logged in
// function fancysquares_stop_guestes( $content ) {
//     global $post;

//     if ( $post->post_type == 'portfolio_piece' ) {
//         if ( !is_user_logged_in() ) {
//             $content = 'Please login to view this post';
//         }
//     }

//     return $content;
// }

// add_filter( 'the_content', 'fancysquares_stop_guestes' );




// Ajax to get more posts 
/* Load more posts */
add_action('wp_ajax_more_posts', __NAMESPACE__ . '\\ajax_more_posts');
add_action('wp_ajax_nopriv_more_posts', __NAMESPACE__ . '\\ajax_more_posts');
function ajax_more_posts()
{ 


    $paged = $_REQUEST['pageCount'];
    ob_start();

  

  $args = [
    'post_type' => 'portfolio_piece',
    'posts_per_page' => 6,
    'post_status' => 'publish'
  ];

  if($paged != "")
    $args['paged'] = $paged; 



  
  $portfolioPieces = new WP_Query( $args ); 

  $html = '';

  if($portfolioPieces->have_posts()) {
    while ($portfolioPieces->have_posts()) : $portfolioPieces->the_post(); 
      include(locate_template( 'partials/portfolio/post-login-preview.php' ));
    endwhile;
  } else {
    echo "nomore";
  }
  
  wp_reset_query();
  wp_reset_postdata();

  $message =  ob_get_clean();
  echo json_encode(['message' => $message]);
  die();

}