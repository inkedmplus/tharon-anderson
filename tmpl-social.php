<?php
/**
 * Template Name: Social
 */
?>

<main class="social-layout">

	<div class="social-layout__container">
	<?php	
			if(($instagramObject = get_transient("tad_instagram")) === false) 
			{			  
				$userid = 9369887;
				$clientid = 'd3af0ece3f1a4cd1b52be8c81ffdaaf8';
				$num_photos = 18; 
				$instagramAPI = file_get_contents('https://api.instagram.com/v1/users/' . $userid . '/media/recent?access_token=9369887.1677ed0.3d654c66c0dd4f24bb1379977084819e&client_id:'.$clientid.'&count:'.$num_photos);
				$instagramObject = json_decode( $instagramAPI , true );
				set_transient("tad_instagram", $instagramObject, 3600);
			}
			
		// If we have instagramObject
		if ( false !== $instagramObject ) : ?>

		   	<?php //print_r($instagramObject['data']);?>
		   	<ul>

			   <?php foreach( $instagramObject['data'] as $image ):?>
				   <li class="instagram-list">
					   	<div class="instagram-list__item">
					   		<a href="<?php echo $image['link'];?>" class="instagram-list__likes" target="_blank">
					   			<span class="wrap">
									<!-- <i class="fa fa-heart" aria-hidden="true"></i> -->
									<span class="count">
										<?php echo esc_html('Follow On');?></br>
										<?php echo esc_html('Instagram');?>
									</span>
								</span>
					   		</a>
					   		<img src="<?php echo $image['images']['standard_resolution']['url'];?>">
					   	</div>
				   	</li>
			   <?php endforeach; ?>
			</ul>
		<?php endif;?>
	</div>
</main>