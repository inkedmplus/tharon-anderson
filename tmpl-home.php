<?php
/**
 * Template Name: Home
 */
?>

<!-- slider -->
<?php get_template_part('partials/home/slider'); ?>

<!-- intro -->
<?php get_template_part('partials/home/intro'); ?>

<!-- testimonial -->
<?php get_template_part('partials/home/testimonial'); ?>


<!-- news and press -->
<?php get_template_part('partials/home/news-and-press'); ?>

<!-- mailing list-->
<?php get_template_part('partials/home/mailing-list'); ?>