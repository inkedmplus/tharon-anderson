var OwlInstagram = (function ($) {

	

    function init() {

    	$('#owlInstagram').owlCarousel({
				    loop:true,
				    margin:0,
				    nav:true,
				    responsive:{
				        0:{
				            items:1
				        },
				        600:{
				            items:3
				        },
				        1000:{
				            items:6
				        },
				        1700:{
				            items:8
				        },
				        2500:{
				            items:10
				        }

				    }
				});

    }

    


    return {
        init: init
    };
    

})(jQuery);
