var LoadMore = (function ($) {

	

    function init() {

    	$('.loadmore').click(function(e){
						e.preventDefault();
						var pageCount = $(this).attr('data-page');
						var nextPage  = parseInt( $(this).attr('data-page') ) + 1;

						if($(this).hasClass('shake')) {
							(this).removeClass('shake');
						}

						$.ajax({
							dataType: 'JSON',
							url: ajaxurl,
							data: {
								'action':'more_posts',
								'pageCount': pageCount
							},
							success: function(data){
								// console.log(data.message);
								if(data.message === "nomore") {
									console.log('none');
								} else {
									$('#all-portfolio-pieces').append(data.message);
								}

							},
							complete: function (data) {
								// console.l
								// Schedule the next
								//setTimeout(updateCartCount, interval);

								$('.loadmore').attr('data-page', nextPage);


								console.log(data);
								console.log(data.message);
							}
						});
					});
    }

    


    return {
        init: init
    };
    

})(jQuery);