var OwlJSHome = (function ($) {

	

    function init() {

    	$('#owlHome').owlCarousel({
		    center: true,
		    items:2,
		    loop:true,
		    margin:30,
		    nav:true,
		    autoHeight:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        768:{
		            items:3
		        }
		    }
		});


		$('#owlTestimonialHome').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    autoHeight:true,
		    responsive:{
		        0:{
		            items:1
		        }
		    }
		});
    }

    


    return {
        init: init
    };
    

})(jQuery);

