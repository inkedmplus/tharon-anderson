<?php
/**
 * Template Name: Portfolio
 */
?>


<?php
	$args = array(            
		'post_type' => 'portfolio_piece',
		'posts_per_page' => -1,
		'post_status' => 'publish'
	);
	//$query = query_posts($args);
	$portfolioPieces = new WP_Query( $args );		
?>
<div id="all-portfolio-pieces" class="portfolio-pieces">
<?php  
	while ($portfolioPieces->have_posts()) : $portfolioPieces->the_post();
	
?>
	
	

			<?php get_template_part('partials/portfolio/preview'); ?>

	


<?php 
	endwhile; 
	wp_reset_query();
?>

	
</div>
<div class="text-center">
<?php
$hasher        = new PasswordHash( 8, true );
$post_password = 'tadesign';
$hash          = isset( $_COOKIE[ 'wp-postpass_' . COOKIEHASH ] ) ? $_COOKIE[ 'wp-postpass_' . COOKIEHASH ] : false;
?>
	<?php if($hash && $hasher->CheckPassword( $post_password, $hash)):?>
    	<div class="view">
			<a data-page="2" class="loadmore button-types button-types--main hide" >View More</a>
		</div>
    <?php else:?>
    	<div class="view">
			<a data-open="exampleModal1" class="button-types button-types--main hide" >View More</a>
		</div>
	<?php endif;?>
</div>

<div class="reveal reveal-plus" id="exampleModal1" data-reveal>
	 <h2>
	 	<?php echo esc_html('Password Protected');?>
	 </h2>
	 <p>
	 	<?php echo esc_html('This project is not for the publics eye just yet. Please enter the password you’ve received from Tharon Anderson Design to view.');?>
	 </p>

	<?php echo get_the_password_form(  ); ?>
<!-- 	<a class="button" data-open="exampleModal2">enter password</a> -->
	<a class="button" data-open="exampleModal2">Request Password</a>
</div>
<div class="reveal reveal-plus" id="exampleModal2" data-reveal>
	<h2>
	 	<?php echo esc_html('Password Protected');?>
	 </h2>
	 <p>
	 	<?php echo esc_html('This project is not for the publics eye just yet. Please enter the password you’ve received from Tharon Anderson Design to view.');?>
	 </p>
	<?php echo gravity_form(2, false, false, false, '', true )?>
</div>
