<?php
/**
 * Template Name: Press
 */
?>

<main class="press-layout">

	<section class="press-layout__content">
		<?php
				$args = array(            
					'post_type' => 'news_and_press',
					'posts_per_page' => -1
				);
				//$query = query_posts($args);
				$newsPressHome = new WP_Query( $args );		
			?>
			<div class="misc-wrap misc-wrap--template">
				<div class="news-and-press news-and-press--home news-and-press--template">
					<h1 class="news-and-press__intro-title">
						<?php the_title();?>
					</h1>

					<?php  
						while ($newsPressHome->have_posts()) : $newsPressHome->the_post();
					?>
						<?php get_template_part('partials/news-press'); ?>
					<?php 
					 	endwhile; 
						wp_reset_query();
					?>
				</div>

				
				
			</div>
	</section>

	<section class="text-center" style="margin-bottom: 75px;">
		<a href="<?php echo get_page_link(149); ?>" class="button-types button-types--main">
					<?php echo esc_html('Contact Us');?>
				</a>
	</section>

</main>