<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript">
		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	</script>
	<?php wp_head(); ?>

	<script src="https://use.typekit.net/moc7oak.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<?php if(!empty($post->post_password)):?>
		<meta name="pinterest"content="nopin" />
	<?php endif;?>
</head>
