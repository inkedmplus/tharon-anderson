<?php
	
	$introImage = get_post_meta( get_the_ID(),'featured_image_intro_portfolio',true);
	$introImage = json_decode($introImage);
	$introImage = $introImage->cropped_image;
	

	$contentLeft = esc_html( get_post_meta( get_the_id(), 'content_left_intro', true ) );
	$contentRight = esc_html( get_post_meta( get_the_id(), 'content_right_intro', true ) );
?>

<?php if( post_password_required()):?>
	<div class="reveal reveal-plus" id="exampleModal1" data-reveal>
	 <h2>
	 	<?php echo esc_html('Password Protected');?>
	 </h2>
	 <p>
	 	<?php echo esc_html('This project is not for the publics eye just yet. Please enter the password you’ve received from Tharon Anderson Design to view.');?>
	 </p>

	<?php echo get_the_password_form(  ); ?>
<!-- 	<a class="button" data-open="exampleModal2">enter password</a> -->
	<a class="button" data-open="exampleModal2">Request Password</a>
</div>
<div class="reveal reveal-plus" id="exampleModal2" data-reveal>
	<h2>
	 	<?php echo esc_html('Password Protected');?>
	 </h2>
	 <p>
	 	<?php echo esc_html('This project is not for the publics eye just yet. Please enter the password you’ve received from Tharon Anderson Design to view.');?>
	 </p>
	<?php echo gravity_form(2, false, false, false, '', true )?>
</div>

<?php else:?>



	<div class="portfolio-layout">

		<section class="portfolio-layout__intro">
			<h1 class="portfolio-layout__intro-title">
				<?php the_title();?>
			</h1>

			<?php if(!empty($post->post_password)){?>
				<?php echo wp_get_attachment_image( $introImage,'full' ); ?>
			<?php }else{?>
				<?php echo wp_get_attachment_image( $introImage,'full','',["class" => "attachment-full size-full pin-attachted-img"] ) ?>
			<?php }?>
			
		</section>

		<section class="portfolio-layout__content" data-equalizer data-equalize-on="medium">
			<div class="portfolio-layout__attributes" data-equalizer-watch> 
				<?php
					$projectAttributes = get_post_meta( get_the_ID(), 'attributes', true );
					if( $projectAttributes ):
				?>	
						<ul class="">
							<?php 
								for( $x = 0; $x < $projectAttributes; $x++ ):
								$attrText = esc_html( get_post_meta( get_the_ID(), 'attributes_' . $x . '_type_of_attribute', true ) );
							?>

								<li class="">
									<?php echo $attrText;?>
								</li>

							<?php endfor;?>
						</ul>

				<?php endif;?>
			</div>

			<div class="portfolio-layout__copy" data-equalizer-watch> 
				<div class="portfolio-layout__content-left">
					<p>
						<?php echo $contentLeft;?>
					</p>
				</div>
				<div class="portfolio-layout__content-right">
					<p>
						<?php echo $contentRight;?>
					</p>
				</div>
			</div>
		</section>

		
		<?php 

		//get relationship field
		//foundation default orbit slider
		$posts = get_post_meta( get_the_ID(), 'select_a_testimonial', true );

		if( $posts ): ?>
			<section class="portfolio-layout__testimonial">
				<div class="testimonial testimonial--portfolio">	
				<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
		            //get your acf meta fields
		            
		         ?>
		            <?php 
		            	setup_postdata($post); 
						$testimonialDescription = esc_html( get_post_meta( get_the_id(), 'description', true ) );
		        	?>

		        		<div class="testimonial__reg-block">
		        			<h2 class="testimonial__title">
		        				“<?php echo $testimonialDescription;?>”
		        			</h2>
		        			<span class="testimonial__cite">
		        				<?php echo esc_html('- ');?> <?php the_title();?>
		        			</span>
		        		</div>
		            
		        <?php endforeach; ?>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				</div>
			</section>

		<?php endif; ?>
			

		<section class="portfolio-layout__images">
			<?php $rows = get_post_meta( get_the_ID(), 'images_intro', true ); ?>
			<?php foreach ( (array) $rows as $index => $row ) : $field_prefix = 'images_intro_' . $index . '_'; ?>
				<?php switch ( $row ) : case 'image_left_image_right' :?>

						<?php
							$imgLeft = get_post_meta( get_the_ID(), $field_prefix . 'image_left', true );
							$imgLeft = json_decode($imgLeft);
							$imgLeft = $imgLeft->cropped_image;
							$imgRight = get_post_meta( get_the_ID(), $field_prefix . 'image_right', true );
							$imgRight = json_decode($imgRight);
							$imgRight = $imgRight->cropped_image;
						?>
						<div class="portfolio-layout__img portfolio-layout__img--double">
							

							<?php if(!empty($post->post_password)){?>

								<?php echo wp_get_attachment_image( $imgLeft,'full' ) ?>
								<?php echo wp_get_attachment_image( $imgRight,'full' ) ?>
							
							<?php }else{?>
							
								<?php echo wp_get_attachment_image( $imgLeft,'full','',["class" => "attachment-full size-full pin-attachted-img"] ) ?>
								<?php echo wp_get_attachment_image( $imgRight,'full','',["class" => "attachment-full size-full pin-attachted-img"] ) ?>
							
							<?php }?>
						</div>

					<?php break; ?>
					<?php case 'full_image' : ?>
						<?php
							$imgFullWidth = get_post_meta( get_the_ID(), $field_prefix . 'full_width_image', true );
							$imgFullWidth = json_decode($imgFullWidth);
							$imgFullWidth = $imgFullWidth->cropped_image;
						?>
						<div class="portfolio-layout__img portfolio-layout__img--solo">

							<?php if(!empty($post->post_password)){?>

								<?php echo wp_get_attachment_image( $imgFullWidth,'full' ) ?>

							<?php }else{?>
								
								<?php echo wp_get_attachment_image( $imgFullWidth,'full','',["class" => "attachment-full size-full pin-attachted-img"] ) ?>
							
							<?php }?>
						</div>

					<?php break; ?>

				<?php endswitch; ?>
			<?php endforeach; ?>
		</section>

		

			
				
				<?php
					$asSeenIn = get_post_meta( get_the_ID(), 'logos_as_seen_in', true );
					if( $asSeenIn ):
				?>	
			<section class="portfolio-layout__logos">
			<div>
				<h2>
					<?php echo esc_html('See it in the press:');?>
				</h2>

					<ul class="">
						<?php 
							for( $z = 0; $z < $asSeenIn; $z++ ):
							$asSeenLogo = (int) get_post_meta( get_the_ID(), 'logos_as_seen_in_' . $z . '_image', true );
							$asSeenLink = esc_attr(get_post_meta( get_the_ID(), 'logos_as_seen_in_' . $z . '_link', true ));
						?>

							<li class="">
								
								<?php if($asSeenLink):?>
									<a href="<?php echo $asSeenLink;?>" target="_blank">
										<?php echo wp_get_attachment_image( $asSeenLogo,'full' ) ?>
									</a>
								<?php else:?>
									<?php echo wp_get_attachment_image( $asSeenLogo,'full' ) ?>
								<?php endif;?>
							</li>

						<?php endfor;?>
					</ul>
				</div>
			</section>
				<?php endif;?>
		
		


		<?php
			$next_post = get_previous_post();
			if($next_post): 

			$introImageNext = get_post_meta( $next_post->ID,'featured_image_intro_portfolio',true);
			$introImageNext = json_decode($introImageNext);
			$introImageNext = $introImageNext->cropped_image;
		?>
			<!-- <section class="portfolio-layout__next-project">
			   <?php $next_title = strip_tags(str_replace('"', '', $next_post->post_title));?>
			   <a class="clearfix" rel="next" href="<?php echo get_permalink($next_post->ID);?>" title="<?php echo $next_title?>" class=" ">
					<span style="display:block;position:relative">
						<span class="portfolio-layout__next-project-img" style="background-image: url(<?php echo wp_get_attachment_url( $introImageNext,'medium' ) ?>);">
						</span>
		

						<span class="portfolio-layout__bg-next">
							<span class="portfolio-layout__middle">
								<span class="portfolio-layout__next-project-subtitle">
									<?php echo esc_attr('Next Project');?>
								</span>

								<span class="portfolio-layout__next-project-title">
									<?php echo $next_title?>
								</span>
							</span>
						</span>
					</span>
			   </a>


			</section> -->
			<section class="portfolio-layout__next-project">
			   <?php $next_title = strip_tags(str_replace('"', '', $next_post->post_title));?>
			   <a class="clearfix" rel="next" href="<?php echo get_permalink($next_post->ID);?>" title="<?php echo $next_title?>" class=" ">
					
					<span class="portfolio-layout__next-project-img" style="background-image: url(<?php echo wp_get_attachment_url( $introImageNext,'medium' ) ?>);">
					</span>

					
					
					<span class="portfolio-layout__next-info">
						<span class="portfolio-layout__center-me">
							<span class="portfolio-layout__next-project-subtitle">
								<?php echo esc_attr('Next Project');?>
							</span>

							<span class="portfolio-layout__next-project-title">
								<?php echo $next_title?>
							</span>
						</span>

					</span>


					
			   </a>


			</section>
		<?php endif?>

	</div>

<?php endif;?>